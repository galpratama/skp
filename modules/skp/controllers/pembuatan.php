<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of pembuatan
 *
 * @author hari
 */
class pembuatan extends CI_Controller{
   public function __construct() {
        parent::__construct();
        $this->ion_auth->check_uri_permissions();
        $this->load->model('skp_model', 'skp_model');
    }

    public function index() {
      $data['list_pegawai'] = $this->skp_model->get_all_pegawai();

      if ($this->input->post('select_peg_id_baru')) {
          $peg_id = $this->input->post('select_peg_id_baru');
      } else if ($this->uri->segment(4)){
          $peg_id = $this->uri->segment(4);
      } else {
        $peg_id = null;
      }

      if($peg_id){

        $data['pegawai'] = $this->skp_model->get_pegawai_by_nik($peg_id);
        $data['skp_pokok'] = $this->skp_model->get_skp_pokok($peg_id);
        $data['skp_tambahan'] = $this->skp_model->get_skp_tambahan($peg_id);
      }
      
        $this->template->load('mainlayout', 'index', $data);
    }

    public function addpokok() {
      $data_input = array(
        'nomor_kegiatan' => $this->input->post('nomor_kegiatan'),
        'deskripsi_kegiatan' => $this->input->post('deskripsi_kegiatan'),
        'c_nik_pgw' => $this->input->post('nikPegawai'),
        'c_nik_pgw_atasan' => $this->input->post('nikAtasan')
        );

      $this->db->insert('public.tminputskp',$data_input);

      redirect('/skp/pembuatan/index/' . $this->input->post('nikPegawai'));
    }

    public function addtambahan() {
      $data_input = array(
        'nomor_kegiatan' => $this->input->post('nomor_kegiatan'),
        'deskripsi_kegiatan' => $this->input->post('deskripsi_kegiatan'),
        'c_nik_pgw' => $this->input->post('nikPegawai'),
        'c_nik_pgw_atasan' => $this->input->post('nikAtasan')
        );

      $this->db->insert('public.tminputskp_tmbhn',$data_input);

      redirect('/skp/pembuatan/index/' . $this->input->post('nikPegawai'));
    }

    public function deletepokok(){

        $this->db->where('id_pengajuan', $this->uri->segment(4));
        $this->db->delete('public.tminputskp');

        redirect('/skp/pembuatan/index/' . $this->uri->segment(5));

    }

    public function deletetambahan(){

        $this->db->where('id_Pengajuan', $this->uri->segment(4));
        $this->db->delete('public.tminputskp_tmbhn');

        redirect('/skp/pembuatan/index/' . $this->uri->segment(5));

    }

    public function tambahkegiatan(){

      $count = count($_POST['pokok']);
      $data = array();
      
      for($i=0; $i<$count; $i++) {
        $data[$i] = array(
          'target_kuantitatif'=>$_POST['pokok'][$i]['target_kuantitatif'],
          'satuan_target_kuantitatif'=>$_POST['pokok'][$i]['satuan_target_kuantitatif'],
          'target_kualitas'=>$_POST['pokok'][$i]['target_kualitas'],
          'waktu'=>$_POST['pokok'][$i]['waktu'],
          'satuan_waktu'=>$_POST['pokok'][$i]['satuan_waktu'],
          'biaya'=>$_POST['pokok'][$i]['biaya'],
          'id_pengajuan'=>$_POST['pokok'][$i]['id_pengajuan'],
          'tgl_pengajuan'=>date('Y-m-d')
          );
        $this->db->where('id_pengajuan', $_POST['pokok'][$i]['id_pengajuan']);
        $this->db->update('public.tminputskp',$data[$i]);
      } 

      // $count2 = count($_POST['tambahan']);
      // $data2 = array();

      // for($i2=0; $i2<$count2; $i2++) {
      //   $data2[$i] = array(
      //     'target_kuantitatif'=>$_POST['tambahan'][$i2]['target_kuantitatif'],
      //     'satuan_target_kuantitatif'=>$_POST['tambahan'][$i2]['satuan_target_kuantitatif'],
      //     'target_kualitas'=>$_POST['tambahan'][$i2]['target_kualitas'],
      //     'waktu'=>$_POST['tambahan'][$i2]['waktu'],
      //     'satuan_waktu'=>$_POST['tambahan'][$i2]['satuan_waktu'],
      //     'biaya'=>$_POST['tambahan'][$i2]['biaya'],
      //     'id_Pengajuan'=>$_POST['tambahan'][$i2]['id_Pengajuan'],
      //     'tgl_pengajuan'=>date('Y-m-d')
      //     );
      //   $this->db->where('id_Pengajuan', $_POST['tambahan'][$i2]['id_Pengajuan']);
      //   $this->db->update('public.tminputskp_tmbhn',$data2[$i2]);
      // } 

      // Debug Sript
      // echo "<pre>";
      // var_dump($data);

      redirect('/skp/pembuatan/index/' . $this->input->post('nikPegawai'));
    }

  
}
