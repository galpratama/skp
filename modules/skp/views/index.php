<!-- Modal -->
<div class="modal fade" id="pilih_pegawai" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Pilih Pegawai</h4>
      </div>
      <div class="modal-body">
        <form role="form" action="<?php echo base_url();?>skp/pembuatan/index" method="post">
        <select name="select_peg_id_baru" width="350px" class="pegawai-select col-sm-12">
        <?php
          foreach ($list_pegawai->result() as $list_pegawai_result) {  ?>
            <option name="select_peg_id_baru" value="<?php echo $list_pegawai_result->peg_nip_baru ?>"><?php echo $list_pegawai_result->peg_nama ?> <?php echo $list_pegawai_result->peg_nip_baru ?></option>
          <?php } ?>
        </select>
        <script charset="utf-8">
        $(window).load(function(){
           $(".pegawai-select").chosen();
         });
        </script>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Pilih Pegawai</button>
      	</form>
      </div>
    </div>
  </div>
</div>


<ul class="page-breadcrumb breadcrumb">
    <li>
        <i class="icon-home"></i>
        <a href="<?php echo base_url(); ?>">Home</a>
        <i class="icon-angle-right"></i>
    </li>
    <li>
        Pembuatan SKP
    </li>
</ul>
<div class="row">
	<form role="form" enctype='multipart/form-data'  action="<?php echo base_url();?>skp/pembuatan/tambahkegiatan" method="post">
	<div class="col-sm-12">
	    <h3>
	    	Form Entry Formulir Sasaran Kinerja Pegawai (SKP)
	    	<button type="button" data-toggle="modal" data-target="#pilih_pegawai" class="btn btn-md btn-primary pull-right"><i class="glyphicon glyphicon-user"></i> Pilih Pegawai Yang Akan Dinilai</button>
	    </h3><br>
	    	<div class='col-sm-6'>
	    		<?php $user = $this->ion_auth->get_data_user_by_id();
	    			  $listuser = $this->ion_auth->get_user_name_all();
	    		?>
	    		<table class="col-sm-12 table table-bordered" id="tblPenilai">
	    			<tr>
	    			   	<th width="10%">No</th>
	    			   	<th colspan="2">I. Pejabat Penilai</th>
	    			</tr>
	    			<tr>
	    				<td>1</td>
	    			    <td width="20%">Nama</td>
	    			    <td><?php echo $user->peg_nama; ?></td>
	    			</tr>
	    			<tr>
	    			    <td>2</td>
	    			    <td>NIK</td>
	    			    <td><?php echo $user->peg_nip_baru; ?><input type="hidden" name="nikAtasan" value="<?php echo $user->peg_nip_baru; ?>"></td>
	    			</tr>
	    			<tr>
	    			    <td>3</td>
	    			    <td>Jabatan</td>
	    			    <td><?php echo $user->jabatan_nama; ?></td>
	    			</tr>
	    			<tr>
	    			    <td>4</td>
	    			    <td>Pangkat</td>
	    			    <td></td>
	    			</tr>
	    			<tr>
	    			    <td>5</td>
	    			    <td>Unit Kerja</td>
	    			    <td><?php echo $user->unit_kerja_nama; ?></td>
	    			</tr>
	    		</table>
	    	</div>
	    	<div class='col-sm-6'>

          <?php if($this->input->post('select_peg_id_baru') or $this->uri->segment(4)) {
            $pegawai_result = $pegawai->row();
            ?>
	    		<table class="col-sm-12 table table-bordered" id="tblDinilai">
	    			<tr>
	    			   	<th width="10%">No</th>
	    			   	<th colspan="2">II. Pegawai Negeri Sipil Yang Dinilai</th>

	    			</tr>
	    			<tr>
	    				<td>1</td>
	    			    <td width="20%">Nama</td>
	    			    <td id="dinilaiName"><?php echo $pegawai_result->peg_nama; ?></td>
	    			</tr>
	    			<tr>
	    			    <td>2</td>
	    			    <td>NIK</td>
	    			    <td id="dinilaiNik"><?php echo $pegawai_result->peg_nip_baru; ?><input type="hidden" name="nikPegawai" value="<?php echo $pegawai_result->peg_nip_baru; ?>"></td>
	    			</tr>
	    			<tr>
	    			    <td>3</td>
	    			    <td>Jabatan</td>
	    			    <td id="dinilaiJab"><?php echo $pegawai_result->jabatan_nama; ?></td>
	    			</tr>
	    			<tr>
	    			    <td>4</td>
	    			    <td>Pangkat</td>
	    			    <td id="dinilaiPan"></td>
	    			</tr>
	    			<tr>
	    			    <td>5</td>
	    			    <td>Unit Kerja</td>
	    			    <td id="dinilaiUK"><?php echo $pegawai_result->unit_kerja_nama; ?></td>
	    			</tr>
	    		</table>
          <?php } else { ?>
            <table class="col-sm-12 table table-bordered" id="tblDinilai">
              <tr>
                   <th width="10%">No</th>
                   <th colspan="2">II. Pegawai Negeri Sipil Yang Dinilai</th>

              </tr>
              <tr>
                <td>1</td>
                <td width="20%">Nama</td>
                <td rowspan="5">
                  <i>Pilih Pegawai Terlebih dahulu</i>
                </td>
              </tr>
              <tr>
                  <td>2</td>
                  <td>NIK</td>
              </tr>
              <tr>
                  <td>3</td>
                  <td>Jabatan</td>
              </tr>
              <tr>
                  <td>4</td>
                  <td>Pangkat</td>
              </tr>
              <tr>
                  <td>5</td>
                  <td>Unit Kerja</td>
              </tr>
            </table>
          <?php } ?>
	   		</div>
	</div>
</div>
<div class='row'>
	<div class='col-sm-12'>
	    <h3>
	    	Kegiatan Tugas Pokok Jabatan
	    </h3>
	    <hr>
      <?php if($this->input->post('select_peg_id_baru') or $this->uri->segment(4)) { ?>
			<table class="table table-bordered table-stripped" id="tblJabatan">
			  <tr>
			    <th width="1%" rowspan="2">No.</th>
			    <th rowspan="2" width="50%">Kegiatan Tugas Pokok Jabatan</th>
			    <th rowspan="2">Angka Kredit</th>
			    <th colspan="4">Target</th>
			    <th rowspan="2" width="5%">Aksi</th>
			  </tr>
			  <tr>
			    <th>Kuantitas <br><i>Output/Satuan</i></th>
			    <th>Kual / Mutu</th>
			    <th>Waktu <br><i>Satuan Waktu</i></th>
			    <th>Biaya</th>
			  </tr>
    		<?php $i = 0; foreach ($skp_pokok->result() as $kegiatan_result) {?>
          <tr>
            <td><?php echo $kegiatan_result->nomor_kegiatan; ?></td>
            <td><?php echo $kegiatan_result->deskripsi_kegiatan; ?></td>
            <td align="center">-</td>
            <td>
            	<input name="pokok[<?php echo $i; ?>][target_kuantitatif]" type="text" class="form-control" style="width: 100px;" value="<?php echo $kegiatan_result->target_kuantitatif; ?>">
            	<input name="pokok[<?php echo $i; ?>][satuan_target_kuantitatif]" type="text" class="form-control" style="width: 100px;" value="<?php echo $kegiatan_result->satuan_target_kuantitatif; ?>">
            </td>
            <td><input name="pokok[<?php echo $i; ?>][target_kualitas]" type="text" class="form-control" style="width: 100px;" value="<?php echo $kegiatan_result->target_kualitas; ?>"></td>
            <td>
            	<input name="pokok[<?php echo $i; ?>][waktu]" type="text" class="form-control" style="width: 100px;" value="<?php echo $kegiatan_result->waktu; ?>">
            	<input name="pokok[<?php echo $i; ?>][satuan_waktu]" type="text" class="form-control" style="width: 100px;" value="<?php echo $kegiatan_result->satuan_waktu; ?>">
            </td>
            <td><input name="pokok[<?php echo $i; ?>][biaya]" type="text" class="form-control" style="width: 100px;" value="<?php echo $kegiatan_result->biaya; ?>"></td>
            <input type="hidden" name="pokok[<?php echo $i; ?>][id_pengajuan]" value="<?php echo $kegiatan_result->id_pengajuan; ?>">
	        <td align="center"><a href="<?php echo base_url();?>skp/pembuatan/deletepokok/<?php echo $kegiatan_result->id_pengajuan; ?>/<?php echo $pegawai_result->peg_nip_baru; ?>" class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-trash"></i> Hapus</a></td>
          </tr>
        <?php $i++; } ?>
        <tr>
		 	<th colspan="8">
		 		<div class='form-group pull-right'>
			 		<button data-toggle="modal" data-target="#modal_pokok_jabatan" type="reset" class="btn btn-success btn-sm "><i class="glyphicon glyphicon-plus"></i> Tambah Kegiatan</button>
		 		</div>
		 	</th>
	 	</tr>
      </table>
    <?php } else { ?>
      <i>Silahkan pilih pegawai terlebih dahulu</i>
    <?php } ?>
  	</div>
	<div class="col-sm-12">
	    <h3>Kegiatan Tugas Pokok Tambahan</h3>
	    <hr>
	    <?php if($this->input->post('select_peg_id_baru') or $this->uri->segment(4)) { ?>
			<table class="table table-bordered table-stripped" id="tblTambahan">
			  <tr>
			    <th width="20px" rowspan="2">No.</th>
			    <th rowspan="2" width="50%">Kegiatan Tugas Pokok Tambahan</th>
			    <th rowspan="2">AK</th>
			    <th colspan="4">Target</th>
			    <th rowspan="2">Aksi</th>
			  </tr>
			  <tr>
			    <th>Kuantitas <br><i>Output/Satuan</i></th>
			    <th>Kual / Mutu</th>
			    <th>Waktu <br><i>Satuan Waktu</i></th>
			    <th>Biaya</th>
			  </tr>
			 <?php $i = 0; foreach ($skp_tambahan->result() as $kegiatan_tambahan_result) {?>
	          <tr>
	            <td><?php echo $kegiatan_tambahan_result->nomor_kegiatan; ?></td>
	            <td><?php echo $kegiatan_tambahan_result->deskripsi_kegiatan; ?></td>
	            <td align="center">-</td>
	            <td>
	            	<input name="tambahan[<?php echo $i; ?>][target_kuantitatif]" type="text" class="form-control" style="width: 100px;" value="<?php echo $kegiatan_tambahan_result->target_kuantitatif; ?>">
	            	<input name="tambahan[<?php echo $i; ?>][satuan_target_kuantitatif]" type="text" class="form-control" style="width: 100px;" value="<?php echo $kegiatan_tambahan_result->satuan_target_kuantitatif; ?>">
	            </td>
	            <td><input name="tambahan[<?php echo $i; ?>][target_kualitas]" type="text" class="form-control" style="width: 100px;" value="<?php echo $kegiatan_tambahan_result->target_kualitas; ?>"></td>
	            <td>
	            	<input name="tambahan[<?php echo $i; ?>][waktu]" type="text" class="form-control" style="width: 100px;" value="<?php echo $kegiatan_tambahan_result->waktu; ?>">
	            	<input name="tambahan[<?php echo $i; ?>][satuan_waktu]" type="text" class="form-control" style="width: 100px;" value="<?php echo $kegiatan_tambahan_result->satuan_waktu; ?>">
	            </td>
	            <td><input name="tambahan[<?php echo $i; ?>][biaya]" type="text" class="form-control" style="width: 100px;" value="<?php echo $kegiatan_tambahan_result->biaya; ?>"></td>
	            <input type="hidden" name="tambahan[<?php echo $i; ?>][id_Pengajuan]" value="<?php echo $kegiatan_tambahan_result->id_Pengajuan; ?>">
		      
	            <td align="center"><a href="<?php echo base_url();?>skp/pembuatan/deletetambahan/<?php echo $kegiatan_tambahan_result->id_Pengajuan; ?>/<?php echo $pegawai_result->peg_nip_baru; ?>" class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-trash"></i> Hapus</a></td>
	          </tr>
	        <?php $i++;} ?>
	        <tr>
			 	<th colspan="8">
			 		<div class='form-group pull-right'>
				 		<button data-toggle="modal" data-target="#modal_pokok_tambahan" type="reset" class="btn btn-success btn-sm "><i class="glyphicon glyphicon-plus"></i> Tambah Kegiatan</button>
			 		</div>
			 	</th>
		 	</tr>
			</table>
	</div>
		<div class="col-sm-12">
			<div class="form-group pull-right">
				<button type="submit" class="btn btn-primary btn-lg"><i class="glyphicon glyphicon-floppy-saved"></i> Simpan Data</button>
				<button type="reset" class="btn btn-danger btn-lg"><i class="glyphicon glyphicon-trash"></i> Ulangi</button>
			</div>
		</div>
    <?php } else { ?>
      <i>Silahkan pilih pegawai terlebih dahulu</i>
    <?php } ?>
	</form>
</div>

<!-- Modal -->
<div class="modal fade" id="modal_pokok_jabatan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
    <form role="form" action="<?php echo base_url();?>skp/pembuatan/addpokok" method="post">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Tambah Kegiatan Tugas Pokok Jabatan</h4>
      </div>
      <div class="modal-body">
         <div class="form-group">
		    <input type="text" class="form-control col-lg-2 input-lg" name="nomor_kegiatan" placeholder="No.">
		    <input type="text" class="form-control col-lg-10 input-lg" name="deskripsi_kegiatan" placeholder="Masukkan Nama Kegiatan...">
		  </div>
		  <input type="hidden" name="nikPegawai" value="<?php echo $pegawai_result->peg_nip_baru; ?>">
		  <input type="hidden" name="nikAtasan" value="<?php echo $user->peg_nip_baru; ?>">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batalkan</button>
        <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
      </div>
     </form>
    </div>
  </div>
</div>


<!-- Modal -->
<div class="modal fade" id="modal_pokok_tambahan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
    <form role="form" action="<?php echo base_url();?>skp/pembuatan/addtambahan" method="post">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Tambah Kegiatan Tugas Pokok Tambahan</h4>
      </div>
      <div class="modal-body">
         <div class="form-group">
		    <input type="text" class="form-control col-lg-2 input-lg" name="nomor_kegiatan" placeholder="No.">
		    <input type="text" class="form-control col-lg-10 input-lg" name="deskripsi_kegiatan" placeholder="Masukkan Nama Kegiatan...">
		  </div>
		  <input type="hidden" name="nikPegawai" value="<?php echo $pegawai_result->peg_nip_baru; ?>">
		  <input type="hidden" name="nikAtasan" value="<?php echo $user->peg_nip_baru; ?>">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batalkan</button>
        <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
      </div>
     </form>
    </div>
  </div>
</div>

<script src="../static/assets/plugins/bootstrap/js/bootstrap2-typeahead.js"></script>
