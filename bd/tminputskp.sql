/*
Navicat PGSQL Data Transfer

Source Server         : localhost
Source Server Version : 90305
Source Host           : localhost:5432
Source Database       : spg
Source Schema         : public

Target Server Type    : PGSQL
Target Server Version : 90305
File Encoding         : 65001

Date: 2014-10-13 01:53:26
*/


-- ----------------------------
-- Table structure for tminputskp
-- ----------------------------
DROP TABLE IF EXISTS "public"."tminputskp";
CREATE TABLE "public"."tminputskp" (
"c_nik_pgw" varchar(32) COLLATE "default",
"c_nik_pgw_atasan" varchar(32) COLLATE "default",
"nomor_kegiatan" int4,
"deskripsi_kegiatan" text COLLATE "default",
"nilai_angka_kredit" int4,
"target_kuantitatif" int4,
"satuan_target_kuantitatif" text COLLATE "default",
"target_kualitas" int4,
"satuan_target_kualitas" text COLLATE "default",
"waktu" int4,
"satuan_waktu" text COLLATE "default",
"biaya" int4,
"satuan_biaya" text COLLATE "default",
"tgl_pengajuan" date,
"id_pengajuan" int4 DEFAULT nextval('tminputskp_id_pengajuan_seq'::regclass) NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of tminputskp
-- ----------------------------
INSERT INTO "public"."tminputskp" VALUES ('198506102010012012', '198212262009011001', '1', 'Membuat Laporan Operasi Komputer', '1', '9', 'Lembar', '1', null, '1', '2', '3', null, '2014-10-12', '51');
INSERT INTO "public"."tminputskp" VALUES ('198506102010012012', '198212262009011001', '2', 'Membuat dokumentasi file yang tersimpan dalam media komputer', '1', '9', '', '1', null, '2', '3', '1', null, '2014-10-12', '52');
INSERT INTO "public"."tminputskp" VALUES ('198506102010012012', '198212262009011001', '3', 'Membuat hasil laporan perekaman data', '2', '9', '', '2', null, '2', '', '2', null, '2014-10-12', '53');
INSERT INTO "public"."tminputskp" VALUES ('198506102010012012', '198212262009011001', '4', 'Melakukan pemasangan peralatan sistem komputer / sistem jaringan komputer', '1', '9', '', '1', null, '1', '', '1', null, '2014-10-12', '54');
INSERT INTO "public"."tminputskp" VALUES ('198506102010012012', '198212262009011001', '5', 'Melakukan deteksi dan atau memperbaiki kerusakan  jaringan komputer', '2', '9', '9', '2', null, '2', '', '2', null, '2014-10-12', '55');
INSERT INTO "public"."tminputskp" VALUES ('198506102010012012', '198212262009011001', '6', 'Melakukan deteksi dan atau memperbaiki kerusakan sistem jaringan komputer', '1', '9', '', '3', null, '4', '', '5', null, '2014-10-12', '56');

-- ----------------------------
-- Table structure for tminputskp_tmbhn
-- ----------------------------
DROP TABLE IF EXISTS "public"."tminputskp_tmbhn";
CREATE TABLE "public"."tminputskp_tmbhn" (
"nomor_kegiatan" int4,
"deskripsi_kegiatan" text COLLATE "default",
"nilai_angka_kredit" int4,
"target_kuantitatif" int4,
"satuan_target_kuantitatif" text COLLATE "default",
"target_kualitas" int4,
"satuan_target_kualitas" text COLLATE "default",
"waktu" int4,
"satuan_waktu" text COLLATE "default",
"biaya" int4,
"satuan_biaya" text COLLATE "default",
"id_Pengajuan" int4 DEFAULT nextval('"tminputskp_tmbhn_id_Pengajuan_seq"'::regclass) NOT NULL,
"c_nik_pgw" varchar(255) COLLATE "default",
"c_nik_pgw_atasan" varchar(255) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of tminputskp_tmbhn
-- ----------------------------
INSERT INTO "public"."tminputskp_tmbhn" VALUES ('1', 'Melakukan deteksi dan atau memperbaiki kerusakan sistem operasi komputer', '2', '2', null, '2', null, '2', null, '2', '', '9', '198506102010012012', '198212262009011001');
INSERT INTO "public"."tminputskp_tmbhn" VALUES ('2', 'Melakukan verifikasi perekaman data', '2', '3', null, '4', null, '5', null, '6', null, '10', '198506102010012012', '198212262009011001');
INSERT INTO "public"."tminputskp_tmbhn" VALUES ('3', 'Halo', null, null, null, null, null, null, null, null, null, '13', '198506102010012012', '198212262009011001');

-- ----------------------------
-- Alter Sequences Owned By 
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table tminputskp
-- ----------------------------
ALTER TABLE "public"."tminputskp" ADD PRIMARY KEY ("id_pengajuan");
